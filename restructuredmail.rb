require 'uri'

class ReStructuredMail # {{{
	class << self # {{{
		def purge_blockbuffer(txt) # {{{
			out = Array.new
			if txt.count > 0
				out = parse_quotable(txt)
			end
			return out
		end # }}}
		def parse_blockquotes(txt) # {{{
			out         = Array.new
			indentlevel = 0
			blockbuffer = Array.new # Contains blockquote contents ready to
			                        # be sent for further processing.
			0.upto (txt.count - 1) do |lineno|
				r_matchblank = Regexp.new("^[[:space:]]*$")
				r_matchquote = Regexp.new("^(>+)[[:space:]]?(.*)$")
				matchblank   = r_matchblank.match(txt[lineno][1])
				matchquote   = r_matchquote.match(txt[lineno][1])
				if ((matchquote) and (txt[lineno][0] == :text))
					newlevel = matchquote[1].length
					if newlevel > indentlevel
						# Close paragraph if initial level
						# is above zero.
						# Do not open paragraph on consecutive
						# blockquotes, only on the last.
						if indentlevel > 0
							(indentlevel + 1).upto newlevel do |b|
								out.concat purge_blockbuffer(blockbuffer)
								blockbuffer.clear
								out << [:html, "<blockquote class=\"quotelevel" + b.to_s + "\">"]
							end
						else
							(indentlevel + 1).upto newlevel do |b|
								out << [:html, "<blockquote class=\"quotelevel" + b.to_s + "\">"]
							end
						end
					elsif newlevel < indentlevel
						# Reopen paragraph on last level drop,
						# if that one is not the last.
						# And only close first paragraph met.
						(indentlevel - newlevel).downto 1 do |b|
							if (b == (indentlevel - newlevel))
								out.concat purge_blockbuffer(blockbuffer)
								blockbuffer.clear
							end
							if (newlevel > 0) and (b == 1)
								out << [:html, "</blockquote>"]
							else
								out << [:html, "</blockquote>"]
							end
						end
					end
					blockbuffer << [:text, matchquote[2]]
					indentlevel = newlevel
				elsif matchblank
					# Allow blankline without closing everything.
					if (
						(lineno >= (txt.count - 3)) or
						(txt[lineno + 1][0] != :text) or
						(not r_matchquote.match(txt[lineno + 1][1]))
					)
						if indentlevel > 0
							1.upto indentlevel do |b|
								out.concat purge_blockbuffer(blockbuffer)
								blockbuffer.clear
								out << [:html, "</blockquote>"]
							end
						end
						indentlevel = 0
					end
					out << txt[lineno]
				else
					out << txt[lineno]
				end
			end
			# Last call.  We may have dangling
			# unclosed blockquotes.
			if indentlevel > 0
				out.concat purge_blockbuffer(blockbuffer)
				blockbuffer.clear
				out << [:html, "</blockquote>"]
			end
			if indentlevel > 1
				2.upto indentlevel do
					out << [:html, "</blockquote>"]
				end
			end
			return out
		end # }}}
		def parse_lists(txt) # {{{
			listchars      = "-+*"
			out            = Array.new
			listindents    = Array.new
			listlevel      = -1
			lastlistindent = 0
			0.upto (txt.count - 1) do |lineno|
				r_emptyline       = Regexp.new("^([[:space:]]*)$")
				r_matchlistitem   = Regexp.new("^([[:space:]]*)([" + listchars + "])[^" + listchars + "][[:space:]]*(.*)$")
				r_nomatchlistitem = Regexp.new("^([[:space:]]*)([^" + listchars + "[:space:]])(.*)$")
				matchlistitem     = (txt[lineno][0] == :text) ? (r_matchlistitem.match(txt[lineno][1])) : false
				nomatchlistitem   = r_nomatchlistitem.match(txt[lineno][1])
				if matchlistitem
					lastlistindent = matchlistitem[1].length
					if !listindents.include?(lastlistindent)
						listindents << lastlistindent
					end
					newlistlevel = listindents.index(matchlistitem[1].length)
					if newlistlevel < listlevel
						1.upto listlevel - newlistlevel do
							out << [:html, "</p></li></ul>"]
						end
						out << [:html, "</li><li><p>"]
					elsif newlistlevel > listlevel
						1.upto newlistlevel - listlevel do |i|
							if newlistlevel == 0 and i == 1
								out << [:html, "<ul class=\"listlevel" + (newlistlevel + 1).to_s + "\"><li><p>"]
							else
								out << [:html, "</p><ul class=\"listlevel" + (newlistlevel + 1).to_s + "\"><li><p>"]
							end
						end
					else
						out << [:html, "</p></li><li><p>"]
					end
					listlevel = newlistlevel
					out << [:text, matchlistitem[3]]
				end
				if not matchlistitem or (lineno == (txt.count - 1))
					# Close all the lists if we get something at a
					# lower level that has no listchar or two blank lines.
					if listlevel > -1
						if (
							(lineno == (txt.count - 1)) or
							(r_emptyline.match(txt[lineno][1]) and r_emptyline.match(txt[lineno + 1][1]))
						)
							out << txt[lineno]
							out << [:html, "</p></li></ul>"]
							# Lowel levels do not need paragraph closing.
							if (listlevel > 0)
								1.upto listlevel do
									out << [:html, "</li></ul>"]
								end
							end
							listindents.clear
							listlevel      = -1
							lastlistindent = 0
						elsif (nomatchlistitem and nomatchlistitem[1].length < lastlistindent)
							0.upto listlevel do
								out << [:html, "</p></li></ul>"]
							end
							out << [:text, ""]
							out << txt[lineno]
							listindents.clear
							listlevel      = -1
							lastlistindent = 0
						else
							# We may need to break a paragraph if this current
							# line is empty and followed by text that is not
							# less indented that the present one.  If so,
							# avoid printing the empty line so that the
							# paragraph is not double-detected.
							followinglinematch = r_nomatchlistitem.match(txt[lineno + 1][1])
							if (
								r_emptyline.match(txt[lineno][1]) and
								txt[lineno + 1][0] == :text and
								followinglinematch and
								followinglinematch[1].length >= lastlistindent
							)
								out << [:html, "</p><p>"]
							else
								out << txt[lineno]
							end
						end
					else
						out << txt[lineno]
					end
				end
			end
			return out
		end # }}}
		def parse_signature(txt) # {{{
			out            = Array.new
			# Check first if a signature exists.
			signature_exists = false
			(txt.count - 1).downto 0 do |lineno|
				if /^--[[:space:]]$/.match(txt[lineno][1])
					signature_exists = true
					break
				end
			end
			if signature_exists
				out << [:html, "</pre></div>"]
				(txt.count - 1).downto 0 do |lineno|
					if /^--[[:space:]]$/.match(txt[lineno][1])
						out << [:text, "-- "]
						out << [:html, "<div class=\"signature\"><pre>"]
					else
						out << txt[lineno]
					end
				end
				out.reverse!
			else
				out = txt
			end
			return out
		end # }}}
		def debug_print(txt) # {{{
			0.upto (txt.count - 1) do |lineno|
				puts (lineno).to_s + " " + txt[lineno][0].to_s + " " + txt[lineno][1]
			end
		end # }}}
		def parse_code(txt) # {{{
			out    = Array.new
			lineno = 0
			while lineno < txt.count
				if (txt[lineno][0] == :text) and /^```$/.match(txt[lineno][1])
					# Check for an ending to this.
					codelineno = lineno + 1
					willclose  = false
					while codelineno < txt.count
						if (txt[codelineno][0] == :text) and /^```$/.match(txt[codelineno][1])
							willclose = true
							break
						end
						codelineno += 1
					end
					if !willclose
						out << txt[lineno]
						lineno += 1
						next
					end
					out << [:html, "<pre class=\"code\">"]
					lineno       += 1
					recodelineno  = lineno
					while recodelineno < codelineno
						out << [:code, txt[recodelineno][1].gsub("<", "&#60;").gsub(">", "&#62;")]
						lineno       += 1
						recodelineno += 1
					end
					out << [:html, "</pre>"]
					lineno += 1
				else
					out << txt[lineno]
					lineno += 1
				end
			end
			return out
		end # }}}
		def parse_newlines(txt) # {{{
			out          = Array.new
			lineno       = 0
			previousline = [:text, "-"]
			while lineno < txt.count
				if previousline[0] == :text and previousline[1] == '' and txt[lineno][0] == :text and txt[lineno][1] == ''
					out << [:html, '<br />']
				else
					out << txt[lineno]
				end
				previousline = txt[lineno]
				lineno += 1
			end
			return out
		end # }}}
		def parse_paragraph(txt) # {{{
			out            = Array.new
			inparagraph    = false
			lineno         = 0
			while lineno < txt.count
				texthasstarted = false
				if /^$/.match(txt[lineno][1]) or ((lineno == 0) and (txt[lineno][0] == :text))
					# Check for an ending to this.
					willclose = false
					if ((txt[lineno][0] == :text) and (not (/^$/.match(txt[lineno][1]))))
						texthasstarted = true
					end
					if (lineno + 1) > (txt.count - 1)
						willclose = true
					else
						(lineno + 1).upto (txt.count - 1) do |nextemptyline|
							if not (/^$/.match(txt[nextemptyline][1]))
								if (txt[nextemptyline][0] != :text)
									willclose = false
									break
								end
								texthasstarted = true
								# Do not exhaust everything, in case paragraph
								# ends at end of file.
								if nextemptyline != (txt.count - 1)
									next
								end
							end
							if texthasstarted and /^$/.match(txt[nextemptyline][1])
								willclose = true
								break
							elsif texthasstarted and nextemptyline == (txt.count - 1)
								# End of text is good too.
								willclose = true
								break
							end
						end
					end
					if willclose
						# Output empty lines as is until we get something real.
						while /^$/.match(txt[lineno][1])
							out << txt[lineno]
							lineno += 1
						end
						out << [:html, "<p>"]
						lineno.upto (txt.count - 1) do |nextline|
							if /^$/.match(txt[nextline][1])
								out << [:html, "</p>"]
								lineno -= 1
								texthasstarted = false
								inparagraph    = false
								break
							else
								out << txt[nextline]
								# Do not forget to close paragraph
								# if at end of file.
								if nextline == (txt.count - 1)
									out << [:html, "</p>"]
								end
							end
							lineno += 1
						end
					else
						out << txt[lineno]
					end
				else
					out << txt[lineno]
				end
				lineno += 1
			end
			return out
		end # }}}
		def parse_hr(txt) # {{{
			out = Array.new
			0.upto (txt.count - 1) do |lineno|
				if /^---$/.match(txt[lineno][1])
					out << [:html, "<hr />"]
				else
					out << txt[lineno]
				end
			end
			return out
		end # }}}
		def parse_links(txt) # {{{
			out = Array.new
			0.upto (txt.count - 1) do |lineno|
				# Only parse links in pure text lines.
				if txt[lineno][0] == :text
					out << [
						txt[lineno][0],
						txt[lineno][1].gsub(
							URI::Parser.new.make_regexp(
								[
									'ftp',
									'ftps',
									'git',
									'http',
									'https',
									'mailto',
									'rsync',
									'scp',
									'sftp',
									'ssh',
								]
							),
							'<a href="\0">\0</a>'
						)
					]
				else
					out << txt[lineno]
				end
			end
			return out
		end # }}}
		def parse_inline_code(txt) # {{{
			out = Array.new
			0.upto (txt.count - 1) do |lineno|
				# Only parse inline code in pure text lines.
				if txt[lineno][0] == :text
					out << [
						txt[lineno][0],
						txt[lineno][1].gsub(
							/(`([^`]+)`)/,
							'<code>\2</code>'
						)
					]
				else
					out << txt[lineno]
				end
			end
			return out
		end # }}}
		def parse_bold(txt) # {{{
			out = Array.new
			0.upto (txt.count - 1) do |lineno|
				# Only parse inline code in pure text lines.
				if txt[lineno][0] == :text
					out << [
						txt[lineno][0],
						txt[lineno][1].gsub(
							/(\*([^\*]+)\*)/,
							'<strong>\2</strong>'
						)
					]
				else
					out << txt[lineno]
				end
			end
			return out
		end # }}}
		def parse_headers(txt) # {{{
			out = Array.new
			r_matchheader = Regexp.new("^(#+)[[:space:]]+([^[:space:]].*)$")
			0.upto (txt.count - 1) do |lineno|
				# Only parse headers in pure text lines.
				if txt[lineno][0] == :text
					matchheader = r_matchheader.match(txt[lineno][1])
					if matchheader
						# HTML pretty much limits header levels to 6.
						matchlength = matchheader[1].length
						if matchlength <= 6
							out << [:html, "<h" + matchlength.to_s + ">"]
							out << [txt[lineno][0], matchheader[2]]
							out << [:html, "</h" + matchlength.to_s + ">"]
						else
							out << txt[lineno]
						end
					else
						out << txt[lineno]
					end
				else
					out << txt[lineno]
				end
			end
			return out
		end # }}}
		def parse_quotable(txt) # {{{
			out = txt
			out = ReStructuredMail.parse_code(out)
			out = ReStructuredMail.parse_headers(out)
			out = ReStructuredMail.parse_lists(out)
			out = ReStructuredMail.parse_hr(out)
			out = ReStructuredMail.parse_paragraph(out)
			out = ReStructuredMail.parse_links(out)
			out = ReStructuredMail.parse_inline_code(out)
			return out
		end # }}}
		def parse_all(txt) # {{{
			out = txt
			out = ReStructuredMail.parse_signature(out)
			out = ReStructuredMail.parse_blockquotes(out)
			out = ReStructuredMail.parse_quotable(out)
			out = ReStructuredMail.parse_newlines(out)
			return out
		end # }}}
		def trim_mail_start(txt) # {{{
			out      = Array.new
			trimming = true
			0.upto (txt.count - 1) do |lineno|
				if not trimming or not (/^$/.match(txt[lineno][1]))
					out << txt[lineno]
					trimming = false
				end
			end
			return out
		end # }}}
		def trim_mail_end(txt) # {{{
			out      = Array.new
			trimming = true
			(txt.count - 1).downto 0 do |lineno|
				if not trimming or not (/^$/.match(txt[lineno][1]))
					out << txt[lineno]
					trimming = false
				end
			end
			return out.reverse
		end # }}}
		def add_html_head_and_body(txt) # {{{
			out = Array.new
			out << [:html, "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"]
			out << [:html, "<html>"]
			out << [:html, "\t<head>"]
			out << [:html, "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"]
			out << [:html, "\t\t<meta charset=\"utf-8\" />"]
			out << [:html, "\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=yes\" />"]
			out << [:html, "\t\t<title></title>"]
			out << [:html, "\t</head>"]
			out << [:html, "\t<body class=\"email\">"]
			0.upto (txt.count - 1) do |lineno|
				out << txt[lineno]
			end
			out << [:html, "\t</body>"]
			out << [:html, "</html>"]
			return out
		end # }}}
		def convert_from_rsm_format(txt) # {{{
			out = String.new
			0.upto (txt.count - 1) do |lineno|
				case txt[lineno][0]
				when :code, :html, :text
					out += txt[lineno][1] + "\n"
				else
					out += "Unsupported line type " + txt[lineno][0].to_s + " on line " + lineno.to_s + "\n"
				end
			end
			return out
		end # }}}
		def convert_to_rsm_format(txt) # {{{
			out = txt.map { |x| [:text, x.chomp] }
			out = ReStructuredMail.trim_mail_start(out)
			out = ReStructuredMail.trim_mail_end(out)
			return out
		end # }}}
		def run(txt, with_html_head_and_body = true) # {{{
			out = ReStructuredMail.convert_to_rsm_format(txt)
			out = ReStructuredMail.parse_all(out)
			if with_html_head_and_body
				out = ReStructuredMail.add_html_head_and_body(out)
			end
			out = ReStructuredMail.convert_from_rsm_format(out)
			return out
		end # }}}
	end # }}}
end # }}}
