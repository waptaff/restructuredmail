require_relative '../restructuredmail'

RSpec.describe ReStructuredMail do
	context "document parsing" do
		it "works for simple paragraph [01]" do
			input  = File.readlines("tests/01.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/01.html")
		end
		it "works for simple quote then paragraph [02]" do
			input  = File.readlines("tests/02.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/02.html")
		end
		it "works for two-level quote then paragraph [03]" do
			input  = File.readlines("tests/03.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/03.html")
		end
		it "works for multiple paragraphs [04]" do
			input  = File.readlines("tests/04.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/04.html")
		end
		it "works for simple tight list [05]" do
			input  = File.readlines("tests/05.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/05.html")
		end
		it "works for simple loose list [06]" do
			input  = File.readlines("tests/06.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/06.html")
		end
		it "works for simple loose two-level list [07]" do
			input  = File.readlines("tests/07.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/07.html")
		end
		it "works for simple tight two-level list [08]" do
			input  = File.readlines("tests/08.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/08.html")
		end
		it "works for horizontal line between two paragraphs [09]" do
			input  = File.readlines("tests/09.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/09.html")
		end
		it "works for five one-line paragraphs [10]" do
			input  = File.readlines("tests/10.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/10.html")
		end
		it "works for list followed by paragraph [11]" do
			input  = File.readlines("tests/11.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/11.html")
		end
		it "works for one-line quotes with extra newline followed by paragraph [12]" do
			input  = File.readlines("tests/12.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/12.html")
		end
		it "works for one-line list items with extra newline ending at level 2 followed by paragraph [13]" do
			input  = File.readlines("tests/13.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/13.html")
		end
		it "works for document ending with random level quoting levels [14]" do
			input  = File.readlines("tests/14.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/14.html")
		end
		it "works for simple URI inside simple paragraph [15]" do
			input  = File.readlines("tests/15.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/15.html")
		end
		it "works for simple headers [16]" do
			input  = File.readlines("tests/16.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/16.html")
		end
		it "works for simple code block [17]" do
			input  = File.readlines("tests/17.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/17.html")
		end
		it "works for code block containing HTML [18]" do
			input  = File.readlines("tests/18.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/18.html")
		end
		it "works for simple code block containing inline code, and standalone inline code [19]" do
			input  = File.readlines("tests/19.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/19.html")
		end
		it "works for lists inside blockquotes [20]" do
			input  = File.readlines("tests/20.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/20.html")
		end
		it "works for quoted mail and signature [21]" do
			input  = File.readlines("tests/21.txt")
			output = ReStructuredMail.run(input, false)
			expect(output).to eq File.read("tests/21.html")
		end
	end
end
