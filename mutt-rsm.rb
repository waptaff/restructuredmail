#!/usr/bin/env ruby27

require          'roadie'
require_relative 'restructuredmail'

cssfile = File.join(ENV['HOME'], '/.restructuredmail.css')

txt = ARGF.readlines
css = ""
if File.exist?(cssfile)
	css = File.read(cssfile)
end

document      = Roadie::Document.new ReStructuredMail.run(txt)
document.mode = :xhtml
document.add_css(css)
puts "text/html\n\n"
puts document.transform

