# ReStructuredMail converts text/plain mail to HTML.


## Why another text-based markup language?

ReStructuredText, markdown, AsciiDoc, and so on already exist.  Why bother?

Traditional mail formatting does not work well with the existing tools except
for very simple cases.  For instance, mail typically includes a signature block
at the end, which unless proven otherwise is not supported by traditional
text markup languages.

Also, due to the subpar HTML support by mail clients, special care must be
taken so that layout directives just don't get ignored; HTML mail layout is
still pretty-much table-oriented.


## Anything else to say?

The tool is currently very primitive, often changing and oriented towards the
author's style and needs.  Use at your own risk.


## Requirements

The `roadie` library is needed to inject CSS into the mail.


## Usage as a library

The `restructuredmail.rb` file can be used as a library; all methods are
static.


## Testing

Running `rspec` should return no error.

Files in the `tests` are simply `.txt` for user input and `.html` for the
expected output HTML, described in `spec/restructuredmail_spec.rb`.


## Usage within mutt

There is a sample `mutt-rsm.rb` that can be used by mutt, if a recent-enough
mutt is installed and the following configuration directives are set in mutt:

```
set send_multipart_alternative=yes
set send_multipart_alternative_filter=/path/to/mutt-rsm.rb
```

If a `.restructuredmail.css` file is found in the home directory, it will be
used to style the e-mail.

